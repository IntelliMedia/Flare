﻿//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;

using Flare.Display;
using Flare.Events;
using Event = Flare.Events.Event;

public class MovieClipExample : MonoBehaviour
{
    public SwfAsset swfAsset;

    private SwfCameraOverlayPlayer player;
    private Flare.Text.TextField textfield;

    void Awake()
    {
        // Turn off Flare logging
        Flare.Log.SubsystemFilter = Flare.Subsystem.None;

        // Get a reference to the camera overlay
        this.player = GetComponent<SwfCameraOverlayPlayer>();
    }

	void Start()
    {
	    if (this.swfAsset == null)
        {
            Debug.Log("SwfAsset is null!");
        }
        else
        {
            // Create a native window with the SWF content
            NativeWindow window = new NativeWindow(this.player.bounds,
                this.player.transparent, this.swfAsset.Bytes);

            // Activate the window so it receives mouse and keyboard events
            window.Activate();

            // Before accessing any Flare API functionalities a NativeWindow must be activiated
            // for script execution. This ensures that any loaded content and events are associated
            // with the correct context. Flare automatically activites the window during event
            // processing; however, during initialization it might be necessary for the application
            // to do it explictly. The following ActivateForScripts isn't really required since
            // Flare implicitly activites the window once it is constructed.
            window.ActivateForScripts();

            // Get the main timeline from the stage (it's named "root1")
            MovieClip main = window.stage.GetChildByName("root1") as MovieClip;

            // Tell the main movie clip to stop playing (it's playing by default)
            main.Stop();

            // Associated the window with the SwfCameraOverlayPlayer
            this.player.nativeWindow = window;

            // Add an event listener to the stage to handle mouse down events
            this.player.nativeWindow.stage.AddEventListener(MouseEvent.MOUSE_DOWN,
                new EventListener(MouseDownCallback));

            // Add a textfield object to the stage to display a message to the user
            this.textfield = new Flare.Text.TextField();
            this.textfield.x = 10;
            this.textfield.y = 10;
            this.textfield.width = 600;
            this.textfield.height = 48;
            this.textfield.htmlText = "<font face='Liberation Sans' size='24'>" +
                "Click the left mouse button to play the movie.</font>";
            this.player.nativeWindow.stage.AddChild(this.textfield);
        }
	}
	
    public void MouseDownCallback(Event evt)
    {
        Stage stage = evt.currentTarget as Stage;

        // Get the main timeline from the stage (it's named "root1")
        MovieClip main = stage.GetChildByName("root1") as MovieClip;

        // If the movie clip isn't already playing then start playing
        if (!main.isPlaying)
        {
            main.GotoAndPlay(1);
        }
    }

    public void Update()
    {
        // Update the visibility of the message based on if the movie is playing
        MovieClip main = this.player.nativeWindow.stage.GetChildByName("root1") as MovieClip;
        this.textfield.visible = !main.isPlaying;
    }
}
