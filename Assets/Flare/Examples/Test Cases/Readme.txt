This folder contains a set of test cases for exercising different features of Flare. These test cases use "programmer art" and are primarily designed for internal testing. The code is rather ugly and needs to be reorganized for future development and testing needs. 

Since this example pre-dates the SwfAsset importer, all of the test case SWFs are located in the Resources folder and have been renamed with a ".bytes" extension instead of a ".swf" extension. This allows them to be loaded as a TextAsset in Unity.

