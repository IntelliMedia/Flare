Shader "Flare/GradientFill" {
    Properties {
        _MainTex ("Texture (RGBA)", 2D) = "white" {}
        _Color ("Fill Color", Color) = (1,1,1,1)
    }

    SubShader {

        Tags { "Queue"="Overlay" "IgnoreProjector"="True" "RenderType"="Transparent" }
        Lighting Off Cull Off ZTest Always ZWrite Off Fog { Mode Off }
        Blend One OneMinusSrcAlpha
        
        // LINEAR PAD 
        Pass {  
			Stencil {
				Ref 0
				Comp Equal
				Pass Keep
			}

            CGPROGRAM
            #pragma target 2.0
            #pragma vertex vert
            #pragma fragment frag
            #pragma fragmentoption ARB_precision_hint_fastest

            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
            };

            struct v2f {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            sampler2D _MainTex;
            uniform fixed4 _Color;
            uniform float4x4 _GradientMatrix;
            
            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = mul(_GradientMatrix, v.vertex).xy / 32768;
                o.texcoord += 0.5;
                return o;
            }
           
            fixed4 frag (v2f i) : COLOR
            {
                // LINEAR PAD
                i.texcoord = clamp(i.texcoord, 0, 1);
                return tex2D(_MainTex, i.texcoord) * _Color;
            }
            ENDCG 
        }
        
        // LINEAR REFLECT 
        Pass {
			Stencil {
				Ref 0
				Comp Equal
				Pass Keep
			}

            CGPROGRAM
            #pragma target 2.0
            #pragma vertex vert
            #pragma fragment frag
            #pragma fragmentoption ARB_precision_hint_fastest

            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
            };

            struct v2f {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            sampler2D _MainTex;
            uniform fixed4 _Color;
            uniform float4x4 _GradientMatrix;
            
            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = mul(_GradientMatrix, v.vertex).xy / 32768;
                o.texcoord += 0.5;
                return o;
            }
           
            fixed4 frag (v2f i) : COLOR
            {
                // LINEAR REFLECT
                i.texcoord.x = frac(i.texcoord.x / 2.0);
                i.texcoord.x = (i.texcoord.x < 0.5) ? (i.texcoord.x * 2) :
                    lerp(1.0, 0.0, (i.texcoord.x - 0.5) * 2);
                return tex2D(_MainTex, i.texcoord) * _Color;
            }
            ENDCG 
        }
        
        // LINEAR REPEAT
        Pass {
			Stencil {
				Ref 0
				Comp Equal
				Pass Keep
			}

            CGPROGRAM
            #pragma target 2.0
            #pragma vertex vert
            #pragma fragment frag
            #pragma fragmentoption ARB_precision_hint_fastest

            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
            };

            struct v2f {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            sampler2D _MainTex;
            uniform fixed4 _Color;
            uniform float4x4 _GradientMatrix;
            
            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = mul(_GradientMatrix, v.vertex).xy / 32768;
                o.texcoord += 0.5;
                return o;
            }
           
            fixed4 frag (v2f i) : COLOR
            {
                // LINEAR REPEAT 
                i.texcoord.x = frac(i.texcoord.x);
                return tex2D(_MainTex, i.texcoord) * _Color;
            }
            ENDCG 
        }

        // For radial gradients the circle equation is given by x^2 + y^2 = 1
        // (it's centered at the origin with a radius of 1). We need to determine
        // where the line (y = mx + b) going through the current pixel and
        // the focal point given by (_FocalPointRatio, 0) intersects with the
        // circle. Plugging the line equation into the circle equation, the
        // quadratic formula is used to compute the intersection.

        // RADIAL PAD 
        Pass {
			Stencil {
				Ref 0
				Comp Equal
				Pass Keep
			}

            CGPROGRAM
            #pragma target 2.0
            #pragma vertex vert
            #pragma fragment frag
            #pragma fragmentoption ARB_precision_hint_fastest

            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
            };

            struct v2f {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            sampler2D _MainTex;
            uniform fixed4 _Color;
            uniform float4x4 _GradientMatrix;
            uniform float _FocalPointRatio;
            
            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = mul(_GradientMatrix, v.vertex).xy / 16384;
                return o;
            } 
           
            fixed4 frag (v2f i) : COLOR
            {
                // RADIAL PAD
                float2 sol;
                float2 fp = float2(_FocalPointRatio, 0);

                if (_FocalPointRatio != i.texcoord.x)
                {
                    float m = i.texcoord.y / (i.texcoord.x - _FocalPointRatio);
                    float b = i.texcoord.y - (m * i.texcoord.x);

                    float A = 1 + (m * m);
                    float B = 2 * b * m;
                    float C = b * b - 1;

                    float D = sqrt((B * B) - (4 * A * C));
                    sol.x = -B + ((i.texcoord.x < _FocalPointRatio) ? -D : D);
                    sol.x /= (2 * A);
                    sol.y = (m * sol.x) + b;
                }
                else
                {
                    sol.x = _FocalPointRatio;
                    sol.y = sqrt(1 - _FocalPointRatio * _FocalPointRatio);
                }

                float ratio = distance(fp, i.texcoord) / distance(fp, sol);

                return tex2D(_MainTex, float2(ratio, 0)) * _Color;
            }
            ENDCG 
        }
        
        // RADIAL REFLECT 
        Pass {
			Stencil {
				Ref 0
				Comp Equal
				Pass Keep
			}

            CGPROGRAM
            #pragma target 2.0
            #pragma vertex vert
            #pragma fragment frag
            #pragma fragmentoption ARB_precision_hint_fastest

            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
            };

            struct v2f {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            sampler2D _MainTex;
            uniform fixed4 _Color;
            uniform float4x4 _GradientMatrix;
            uniform float _FocalPointRatio;
            
            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = mul(_GradientMatrix, v.vertex).xy / 16384;
                return o;
            }

            fixed4 frag (v2f i) : COLOR
            {
                // RADIAL REFLECT
                float2 sol;
                float2 fp = float2(_FocalPointRatio, 0);

                if (_FocalPointRatio != i.texcoord.x)
                {
                    float m = i.texcoord.y / (i.texcoord.x - _FocalPointRatio);
                    float b = i.texcoord.y - (m * i.texcoord.x);

                    float A = 1 + (m * m);
                    float B = 2 * b * m;
                    float C = b * b - 1;

                    float D = sqrt((B * B) - (4 * A * C));
                    sol.x = -B + ((i.texcoord.x < _FocalPointRatio) ? -D : D);
                    sol.x /= (2 * A);
                    sol.y = (m * sol.x) + b;
                }
                else
                {
                    sol.x = _FocalPointRatio;
                    sol.y = sqrt(1 - _FocalPointRatio * _FocalPointRatio);
                }

                float ratio = frac(distance(fp, i.texcoord) / distance(fp, sol) / 2.0);
                ratio = (ratio < 0.5) ? (ratio * 2) : lerp(1.0, 0.0, (ratio - 0.5) * 2);

                return tex2D(_MainTex, float2(ratio, 0)) * _Color;
            }
            ENDCG
        }
        
        // RADIAL REPEAT 
        Pass {
			Stencil {
				Ref 0
				Comp Equal
				Pass Keep
			}

            CGPROGRAM
            #pragma target 2.0
            #pragma vertex vert
            #pragma fragment frag
            #pragma fragmentoption ARB_precision_hint_fastest

            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
            };

            struct v2f {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            sampler2D _MainTex;
            uniform fixed4 _Color;
            uniform float4x4 _GradientMatrix;
            uniform float _FocalPointRatio;
            
            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = mul(_GradientMatrix, v.vertex).xy / 16384;
                return o;
            }

            fixed4 frag (v2f i) : COLOR
            {
                // RADIAL REPEAT
                float2 sol;
                float2 fp = float2(_FocalPointRatio, 0);

                if (_FocalPointRatio != i.texcoord.x)
                {
                    float m = i.texcoord.y / (i.texcoord.x - _FocalPointRatio);
                    float b = i.texcoord.y - (m * i.texcoord.x);

                    float A = 1 + (m * m);
                    float B = 2 * b * m;
                    float C = b * b - 1;

                    float D = sqrt((B * B) - (4 * A * C));
                    sol.x = -B + ((i.texcoord.x < _FocalPointRatio) ? -D : D);
                    sol.x /= (2 * A);
                    sol.y = (m * sol.x) + b;
                }
                else
                {
                    sol.x = _FocalPointRatio;
                    sol.y = sqrt(1 - _FocalPointRatio * _FocalPointRatio);
                }

                float ratio = frac(distance(fp, i.texcoord) / distance(fp, sol));
                return tex2D(_MainTex, float2(ratio, 0)) * _Color;
            }
            ENDCG
        }
    }   

    Fallback off
}
