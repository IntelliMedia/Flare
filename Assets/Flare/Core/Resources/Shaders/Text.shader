Shader "Flare/Text" {
    Properties {
        _MainTex ("Font Texture (A)", 2D) = "white" {}
        _Color ("Text Color", Color) = (1,1,1,1)
        _Clip ("Clip Rect", Vector) = (0,0,0,0)
    }

    SubShader {

        Tags { "Queue"="Overlay" "IgnoreProjector"="True" "RenderType"="Transparent" }
        Lighting Off Cull Off ZTest Always ZWrite Off Fog { Mode Off }
        Blend One OneMinusSrcAlpha

        // Pass for rendering text without clipping
        Pass {
			Stencil {
				Ref 0
				Comp Equal
				Pass Keep
			}

            CGPROGRAM
            #pragma target 2.0
            #pragma vertex vert
            #pragma fragment frag
            #pragma fragmentoption ARB_precision_hint_fastest

            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            uniform sampler2D _MainTex;
            uniform float4 _MainTex_ST;
            uniform fixed4 _Color;
            
            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : COLOR
            {
                fixed4 col = _Color;
                col *= UNITY_SAMPLE_1CHANNEL(_MainTex, i.texcoord);
                return col;
            }
            ENDCG 
        }

        // Pass for rendering text with clipping
        Pass {
			Stencil {
				Ref 0
				Comp Equal
				Pass Keep
			}

            CGPROGRAM
            #pragma target 2.0
            #pragma vertex vert
            #pragma fragment frag
            #pragma fragmentoption ARB_precision_hint_fastest

            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
                float4 modelpos : TEXCOORD1;
            };

            uniform sampler2D _MainTex;
            uniform float4 _MainTex_ST;
            uniform fixed4 _Color;
            uniform float4 _Clip;
            
            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
                o.modelpos = v.vertex;
                return o;
            }

            fixed4 frag (v2f i) : COLOR
            {
                clip((i.modelpos.x < _Clip.x) || (i.modelpos.y < _Clip.y) ||
                    (i.modelpos.x > _Clip.z) || (i.modelpos.y > _Clip.w) ? -1 : 1);
                fixed4 col = _Color;
                col *= UNITY_SAMPLE_1CHANNEL(_MainTex, i.texcoord);
                return col;
            }
            ENDCG 
        }
    }   

    Fallback Off
}
