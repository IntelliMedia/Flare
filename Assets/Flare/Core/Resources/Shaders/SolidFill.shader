Shader "Flare/SolidFill" {
    Properties {
        _Color ("Fill Color", Color) = (1,1,1,1)
    }

    SubShader {

        Tags { "Queue"="Overlay" "IgnoreProjector"="True" "RenderType"="Transparent" }
        Lighting Off Cull Off ZTest Always ZWrite Off Fog { Mode Off }
        Blend One OneMinusSrcAlpha

        Pass {
			Stencil {
				Ref 0
				Comp Equal
				Pass Keep
			}

            Color [_Color]
        }
    }   

    Fallback Off
}
