﻿Shader "Flare/StencilMask" {
    Properties {
		_StencilRef("Stencil Ref", Float) = 0.0
    }

    SubShader {
        
        Tags { "Queue"="Overlay" "RenderType"="Opaque" "IgnoreProjector"="True" }
        Lighting Off Cull Off ZTest Always ZWrite Off Fog { Mode Off }
        ColorMask 0
       
		CGINCLUDE
		#include "UnityCG.cginc"

		float _OffsetZ;
		float _StencilRef;

		ENDCG

		Pass {
			Stencil {
				Ref [_StencilRef]
				Comp Equal
				Pass IncrSat
			}

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			float4 vert(float4 pos : POSITION) : SV_POSITION
			{
				return UnityObjectToClipPos(float4(pos.x, pos.y, pos.z, pos.w));
			}

			fixed4 frag(float4 pos : SV_POSITION) : COLOR0
			{
				return fixed4(1.0, 1.0, 0.0, 1.0);
			}
			ENDCG
		}

		Pass {
			Stencil {
				Ref[_StencilRef]
				Comp Equal
				Pass Zero
				Fail Replace
			}

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			float4 vert(float4 pos : POSITION) : SV_POSITION
			{
				return UnityObjectToClipPos(float4(pos.x, pos.y, pos.z, pos.w));
			}

			fixed4 frag(float4 pos : SV_POSITION) : COLOR0
			{
				return fixed4(1.0, 1.0, 1.0, 1.0);
			}
			ENDCG
		}
    }   

    Fallback Off
}
