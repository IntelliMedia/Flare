Shader "Flare/Line" {
    Properties {
        _Color ("Color (RGBA)", Color) = (1,0,0,1)
        _MainTex ("Filter", 2D) = "white" {}                 // texture containing intensity values
        _decode ("FloatRGBA Decoder", Vector) = (0,0,0,0)    // line segment-specific decoding constants
        _constant ("Decoding Constants", Vector) = (0,0,0,0) // constant decoding constants
        _delta ("Delta", Vector) = (0,0,0,0)                 // width and half-width of texel in _edgeCoeffs
        _edgeCoeffs ("Edge Coefficients", 2D) = "blue" {}    // texture containing encoded edge coefficients
    }
    
    SubShader {
    
        Tags { "Queue"="Overlay" "IgnoreProjector"="True" "RenderType"="Transparent" }
        Lighting Off Cull Off ZTest Always ZWrite Off Fog { Mode Off }
        Blend One OneMinusSrcAlpha
        
        Pass {
			Stencil {
				Ref 0
				Comp Equal
				Pass Keep
			}

            CGPROGRAM
            #pragma target 2.0
            #pragma vertex vert
            #pragma fragment frag
            #pragma fragmentoption ARB_precision_hint_fastest

            #include "UnityCG.cginc"
            
            fixed4 _Color;
            sampler2D _MainTex, _edgeCoeffs;
            float4 _delta;
            float4 _decode, _constant;
            
            struct appdata_t {
                float4 vertex : POSITION;
                float4 texCoord2 : TEXCOORD1;
            };

            struct v2f {
                float4 vertex : POSITION;
                fixed4 color : COLOR;
                float2 coord0 : TEXCOORD0;
                float2 coord1 : TEXCOORD1;
            };
            
            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.coord0 = v.vertex.xy;
                o.coord1 = v.texCoord2.xy;
                o.color = _Color;
                return o;
            }

            float DecodeFRGBA (float4 rgba)
            {
                float x = dot(rgba, _constant);
                x = x * (_decode.y) + _decode.x;
                return x;
            }
            
            fixed4 frag (v2f i) : COLOR
            {
                fixed4 col = i.color;
                float3 pos = float3(i.coord0.x, i.coord0.y, 1.0);

                //_delta.x is the width of a texel; _delta.y is the half-width
                //_delta.z is the height of a texel; _delta.z is the half-height
                float i_index = (i.coord1.x * _delta.x) + _delta.y;

                float3 e0, e1, e2, e3;

                e0.x = DecodeFRGBA(tex2D(_edgeCoeffs, float2(i_index, 0.04166666666)));
                e0.y = DecodeFRGBA(tex2D(_edgeCoeffs, float2(i_index, 0.125)));
                e0.z = DecodeFRGBA(tex2D(_edgeCoeffs, float2(i_index, 0.20833333333)));

                e1.x = DecodeFRGBA(tex2D(_edgeCoeffs, float2(i_index, 0.29166666666)));
                e1.y = DecodeFRGBA(tex2D(_edgeCoeffs, float2(i_index, 0.375)));
                e1.z = DecodeFRGBA(tex2D(_edgeCoeffs, float2(i_index, 0.45833333333)));

                e2.x = DecodeFRGBA(tex2D(_edgeCoeffs, float2(i_index, 0.54166666666)));
                e2.y = DecodeFRGBA(tex2D(_edgeCoeffs, float2(i_index, 0.625)));
                e2.z = DecodeFRGBA(tex2D(_edgeCoeffs, float2(i_index, 0.70833333333)));

                e3.x = DecodeFRGBA(tex2D(_edgeCoeffs, float2(i_index, 0.79166666666)));
                e3.y = DecodeFRGBA(tex2D(_edgeCoeffs, float2(i_index, 0.875)));
                e3.z = DecodeFRGBA(tex2D(_edgeCoeffs, float2(i_index, 0.95833333333)));

                float4 d = float4(dot(e0, pos), dot(e1, pos), dot(e2, pos), dot(e3, pos));
            
                // Calculate the intensity for this fragment from the filter table
                float c0 = clamp(min(d.x, d.z), 0.0f, 1.0f);
                float c1 = clamp(min(d.y, d.w), 0.0f, 1.0f);

                float tmp1 = tex2D(_MainTex, float2(c0, 0.5f)).w;
                float tmp2 = tex2D(_MainTex, float2(c1, 0.5f)).w;
                col = col * (tmp1 * tmp2);

                return col;
            }
            ENDCG
        }
        
        Pass {
			Stencil {
				Ref 0
				Comp Equal
				Pass Keep
			}

            Color [_Color]
        }
        
    } 
    FallBack off
}