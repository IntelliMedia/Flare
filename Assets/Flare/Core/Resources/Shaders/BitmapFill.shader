Shader "Flare/BitmapFill" {
    Properties {
        _MainTex ("Texture (RGBA)", 2D) = "white" { }
        _Color ("Fill Color", Color) = (1,1,1,1)
    }

    SubShader {

        Tags { "Queue"="Overlay" "IgnoreProjector"="True" "RenderType"="Transparent" }
        Lighting Off Cull Off ZTest Always ZWrite Off Fog { Mode Off }
        Blend One OneMinusSrcAlpha

        Pass {
			Stencil {
				Ref 0
				Comp Equal
				Pass Keep
			}

            CGPROGRAM
            #pragma target 2.0
            #pragma vertex vert
            #pragma fragment frag
            #pragma fragmentoption ARB_precision_hint_fastest

            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
            };

            struct v2f {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            sampler2D _MainTex;
            uniform fixed4 _Color;
            uniform float4x4 _TextureMatrix;
            
            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = mul(_TextureMatrix, v.vertex).xy;
                return o;
            }
           
            fixed4 frag (v2f i) : COLOR
            {
                return tex2D(_MainTex, i.texcoord) * _Color;
            }
            ENDCG 
        }
    }   

    Fallback Off
}
