//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//----------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Flare.Display;
using Flare.Geom;
using Flare.Sys;

public class SwfImporter : AssetPostprocessor
{  
    public static void OnPostprocessAllAssets(string[] importedAssets,
        string[] deletedAssets,
        string[] movedAssets,
        string[] movedFromAssetsPaths)
    {
        foreach (string path in importedAssets)
        {
            if (path.EndsWith(".swf", true, null))
            {
                string assetFile = path.Substring(0, path.Length - 4) + ".asset";

                bool createdAsset = false;
                SwfAsset asset = AssetDatabase.LoadAssetAtPath(assetFile,
                    typeof(SwfAsset)) as SwfAsset;
                if (asset == null)
                {
                    asset = ScriptableObject.CreateInstance(typeof(SwfAsset)) as SwfAsset;
                    asset.Version = SwfAsset.CurrentVersionNumber;
                    createdAsset = true;
                }
                else
                {
                    // Asset already exists so remove all of its old sub-assets
                    Object[] objs = AssetDatabase.LoadAllAssetsAtPath(assetFile);
                    foreach (var obj in objs)
                    {
                        // Destory everything except for the main SwfAsset
                        if (!asset.Equals(obj))
                        {
                            Object.DestroyImmediate(obj, true);
                        }
                    }
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                }

                // At this point we have either just created the SwfAsset or have
                // removed all of its old sub-assets. In either case, we now
                // update its bytes and re-create the sub-assets

                // Copy SWF file contents into the SwfAsset
                asset.Bytes = global::System.IO.File.ReadAllBytes(path);

                if (createdAsset)
                {
                    AssetDatabase.CreateAsset(asset, assetFile);
                }
                else
                {
                    EditorUtility.SetDirty(asset);
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                }

                RefreshSwfAssestObjects(asset);
            }
        }
    }

    // TODO 2017/10/22 bwmott:: Ensure textures are properly compressed in the asset so they are not overly large
    // TODO 2017/10/22 bwmott:: Is there a way to remove the mesh / texture data from the SWF so it's not duplicated?
    // TODO 2017/10/22 bwmott:: Revise Flare core so a NativeWindow isn't required to be created for importing

    private static void RefreshSwfAssestObjects(SwfAsset swfAsset)
    {
        List<SwfAsset.BitmapIdAndTexture> bitmapIdTextureList = new List<SwfAsset.BitmapIdAndTexture>();
        List<SwfAsset.SpriteIdAndMeshes> spriteIdMeshesList = new List<SwfAsset.SpriteIdAndMeshes>();

        // Load the SWF file so that we have access to all of the texture, mesh, and audio data
        new NativeWindow(new Rectangle(), false, null, null);
        ApplicationDomain domain = new ApplicationDomain();
        LoaderContext context = new LoaderContext(domain);
        Loader loader = new Flare.Display.Loader();
        loader.Load(swfAsset.Bytes, context);

        // Loop through the loaded objects from the SWF and create corresponding assets for them
        foreach (var i in context.dictionary)
        {
            int id = i.Key;

            if (i.Value is Flare.Display.BitmapData)
            {
                BitmapData bitmap = (BitmapData)i.Value;
                Texture2D texture = bitmap.texture;

                // Compress the texture if it's larger than 256x256
                if (!((texture.width < 256) && (texture.height < 256)))
                {
                    texture.Compress(true);
                }

                bitmapIdTextureList.Add(new SwfAsset.BitmapIdAndTexture(id, texture));
                AssetDatabase.AddObjectToAsset(texture, swfAsset);
                AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(texture));
            }
            else if (i.Value is Flare.Display.Sprite)
            {
                Flare.Display.Sprite sprite = (Flare.Display.Sprite)i.Value;
                Mesh[] meshes = sprite.graphics.InternalGetFilledMeshes();
                spriteIdMeshesList.Add(new SwfAsset.SpriteIdAndMeshes(id, meshes));
                foreach (var mesh in meshes)
                {
                    AssetDatabase.AddObjectToAsset(mesh, swfAsset);
                    Debug.Log(AssetDatabase.GetAssetPath(mesh));
                    AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(mesh));
                }
            }
            else if (i.Value is Flare.Media.Sound)
            {
                // TODO 2017/10/22 bwmott: Consider processing sound assets at edit time as well
            }
        }

        swfAsset.BitmapIdAndTextureArray = bitmapIdTextureList.ToArray();
        swfAsset.SpriteIdAndMeshesArray = spriteIdMeshesList.ToArray();

        EditorUtility.SetDirty(swfAsset);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}
