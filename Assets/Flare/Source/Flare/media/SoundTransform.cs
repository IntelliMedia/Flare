//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

using Flare.Events;
using UnityEngine;

namespace Flare.Media
{
    /// <summary>
    /// The SoundTransform class allows the volume and panning of a sound to be adjusted
    /// during playback.
    /// </summary>
    public class SoundTransform
    {
        /// <summary>
        /// Gets or sets the left-to-right panning of the sound (-1 = full pan to the left,
        /// +1 = full pan to the right).
        /// </summary>
        public float pan { get; set; }

        /// <summary>
        /// Gets or sets the volume (0 = silent, 1 = full volume).
        /// </summary>
        public float volume { get; set; }

        /// <summary>
        /// Initializes a new instance of the SoundTransform class.
        /// </summary>
        public SoundTransform(float volume = 1, float pan = 0)
        {
            this.volume = volume;
            this.pan = pan;
        }
    }
}