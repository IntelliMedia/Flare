//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

using Flare.Events;
using UnityEngine;

namespace Flare.Media
{
    /// <summary>
    /// The SoundChannel class controls playback of a sound.
    /// </summary>
    public class SoundChannel : EventDispatcher
    {
        /// <summary>
        /// Current point being played in milliseconds.
        /// </summary>
        public float position
        {
            get { return sound.time * 1000f; }
        }

        /// <summary>
        /// Gets or sets the sound transform used by the sound channel.
        /// </summary>
        public SoundTransform soundTransform
        {
            get { return m_soundTransform; }
            
            set
            {
                m_soundTransform = (value == null) ? new SoundTransform() : value;
                sound.panStereo = m_soundTransform.pan;
                sound.volume = m_soundTransform.volume;
            }
        }
        
        internal int samplePosition
        {
            get { return sound.timeSamples; }
        }
        
        internal bool isPlaying
        {
            get { return sound != null && sound.isPlaying; }
        }
        
        private SoundTransform m_soundTransform;
        private int loopCount;
        private AudioSource sound;
        
        internal SoundChannel(AudioSource sound, SoundTransform sndTransform)
        {
            // Store the sound and play it
            this.sound = sound;
            soundTransform = sndTransform;
            this.sound.Play();
        }

        /// <summary>
        /// Stop playing the sound.
        /// </summary>
        public void Stop()
        {
            // Stop the sound by destroying it
            Object.Destroy(sound);
        }
    }
}