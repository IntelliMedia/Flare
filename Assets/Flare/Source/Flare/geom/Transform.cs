//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

namespace Flare.Geom
{
    /// <summary>
    /// A transform consists of a color transform and a 2D transformation matrix that
    /// can be applied to a display object.
    /// </summary>
    public class Transform
    {
        /// <summary>
        /// Access the color transform of the transform.
        /// </summary>
        public ColorTransform colorTransform { get; set; }

        /// <summary>
        /// Get the concatenated color transform which represents the combined color
        /// transforms applied to the display object back to the root.
        /// </summary>
        public ColorTransform concatenatedColorTransform { get;
            /* \cond */ internal set; /* \endcond */ }

        /// <summary>
        /// Access the 2D transformation matrix of the transform.
        /// </summary>
        public Matrix matrix { get; set; }

        /// <summary>
        /// Get the concatenated 2D transformation matrix which represents the combined
        /// transforms applied to the display object back to the root.
        /// </summary>
        public Matrix concatenatedMatrix { get; /* \cond */ internal set; /* \endcond */ }

        /// <summary>
        /// Create a new transform with the identity color transform and tranformation matrix.
        /// </summary>
        public Transform()
        {
            this.colorTransform = new ColorTransform();
            this.matrix = new Matrix();
            this.concatenatedMatrix = new Matrix();
            this.concatenatedColorTransform = new ColorTransform();
        }

        /// <summary>
        /// Create a new transform by copying the given transform.
        /// </summary>
        /// <param name="t">The given transform to copy.</param>
        public Transform(Transform t)
        {
            this.colorTransform = new ColorTransform(t.colorTransform);
            this.matrix = new Matrix(t.matrix);
            this.concatenatedMatrix = new Matrix(t.concatenatedMatrix);
            this.concatenatedColorTransform = new ColorTransform(t.concatenatedColorTransform);
        }
    }
}