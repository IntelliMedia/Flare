﻿//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

using Flare.UI;

namespace Flare.Events
{
    /// <summary>
    /// The KeyboardEvent class provides an event that is dispatched into the eventflow
    /// when keyboard events occur.
    /// </summary>
    public class KeyboardEvent : Event
    {
        /// <summary>
        /// Indicates if Alt key is pressed (Option key on Mac, 
        /// Alt key on Windows or Linux).
        /// </summary>
        public bool altKey { get; set; }

        /// <summary>
        /// Character code value of the key pressed or released.
        /// </summary>
        public char charCode  { get; set; }

        /// <summary>
        /// Indicates if the Ctrl key is pressed (Control or Command key on Mac,
        /// Ctrl key on Windows or Linux).
        /// </summary>
        public bool ctrlKey { get; set; }

        /// <summary>
        /// Key code of the key pressed or released.
        /// </summary>
        public Keyboard.KeyCode keyCode { get; set; }

        /// <summary>
        /// Location of the key pressed or released.
        /// </summary>
        public Keyboard.KeyLocation keyLocation { get; set; }

        /// <summary>
        /// Indicates if the shift key is pressed.
        /// </summary>
        public bool shiftKey { get; set; }

        /// <summary>
        /// Create a new instance of the KeyboardEvent class.
        /// </summary>
        public KeyboardEvent(string type, bool bubbles = true, bool cancelable = false,
            char charCode = '\0', Keyboard.KeyCode keyCode = 0,
            Keyboard.KeyLocation keyLocation = Keyboard.KeyLocation.STANDARD,
            bool ctrlKey = false, bool altKey = false, bool shiftKey = false)
            : base(type, bubbles, cancelable)
        {
            this.charCode = charCode;
            this.keyCode = keyCode;
            this.keyLocation = keyLocation;
            this.ctrlKey = ctrlKey;
            this.altKey = altKey;
            this.shiftKey = shiftKey;
        }
        
        public override string ToString()
        {
            return string.Format("[KeyboardEvent type=\"{0}\" bubbles={1} cancelable={2} " +
                "eventPhase={3} charCode={4} keyCode={5} keyLocation={6} " +
                "ctrlKey={7} altKey={8} shiftKey={9}]",
                this.type, this.bubbles, this.cancelable,
                this.eventPhase, (int)this.charCode, (int)this.keyCode, (int)this.keyLocation,
                this.ctrlKey, this.altKey, this.shiftKey);
        }

        /// <summary>
        /// Constant for keyDown event objects.
        /// </summary>
        public const string KEY_DOWN = "keyDown";
        
        /// <summary>
        /// Constant for keyUp event objects.
        /// </summary>
        public const string KEY_UP = "keyUp";
    }
}
