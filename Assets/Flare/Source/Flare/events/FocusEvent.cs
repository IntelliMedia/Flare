﻿//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

using Flare.UI;
using Flare.Display;

namespace Flare.Events
{
    /// <summary>
    /// The FocusEvent class provides an event that is dispatched into the eventflow
    /// when the focus changes from one object to another.
    /// </summary>
    public class FocusEvent : Event
    {
        /// <summary>
        /// The display object affected by the change in focus.
        /// </summary>
        public InteractiveObject relatedObject { get; set; }

        /// <summary>
        /// Create a new instance of the FocusEvent class.
        /// </summary>
        public FocusEvent(string type, bool bubbles = true, bool cancelable = false,
            InteractiveObject relatedObject = null)
            : base(type, bubbles, cancelable)
        {
            this.relatedObject = relatedObject;
        }
        
        public override string ToString()
        {
            return string.Format("[FocusEvent type=\"{0}\" bubbles={1} cancelable={2} " +
                "eventPhase={3} relatedObject={4}]",
                this.type, this.bubbles, this.cancelable,
                this.eventPhase, this.relatedObject);
        }
        
        /// <summary>
        /// Constant for focusIn event objects.
        /// </summary>
        public const string FOCUS_IN = "focusIn";
        
        /// <summary>
        /// Constant for focusOut event objects.
        /// </summary>
        public const string FOCUS_OUT = "focusOut";
    }
}
