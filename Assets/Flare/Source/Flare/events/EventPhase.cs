//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

namespace Flare.Events
{
    /// <summary>
    /// Constants used to indicate the phase of an event in the event flow through
    /// the eventPhase property of the Event class.
    /// </summary>
    public class EventPhase
    {
        /// <summary>
        /// Capturing phase of the event flow where the event is making its way down
        /// the hierarchy to the target.
        /// </summary>
        public const uint CAPTURING_PHASE = 1;

        /// <summary>
        /// Target phase of the event flow where the event arrives at the target.
        /// </summary>
        public const uint AT_TARGET = 2;

        /// <summary>
        /// Bubbling phase of the event flow where the event makes its way back up
        /// the hierarchy.
        /// </summary>
        public const uint BUBBLING_PHASE = 3;
    }
}
