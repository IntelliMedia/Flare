//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

namespace Flare.Text
{
    /// <summary>
    /// Constants used to specify font styles.
    /// </summary>
    public class FontStyle
    {
        /// <summary>
        /// Indicates bold style of a font.
        /// </summary>
        public const string BOLD = "bold";

        /// <summary>
        /// Indicates combined bold and italic style of a font.
        /// </summary>
        public const string BOLD_ITALIC = "boldItalic";

        /// <summary>
        /// Indicates italic style of a font.
        /// </summary>
        public const string ITALIC = "italic";

        /// <summary>
        /// Indicates regular style of a font.
        /// </summary>
        public const string REGULAR = "regular";

        /// <summary>
        /// Returns the style for the given combination of bold and italic flags
        /// </summary>
        public static string GetStyle(bool bold, bool italic)
        {
            if (!bold && !italic)
                return Flare.Text.FontStyle.REGULAR;
            else if (bold && italic)
                return Flare.Text.FontStyle.BOLD_ITALIC;
            else if (bold)
                return Flare.Text.FontStyle.BOLD;
            else
                return Flare.Text.FontStyle.ITALIC;
        }

        /// <summary>
        /// Returns the corresponding Unity FontStyle for a given Flare FontStyle
        /// </summary>
        public static UnityEngine.FontStyle GetUnityFontStyle(string fontStyle)
        {
            switch (fontStyle)
            {
                case Flare.Text.FontStyle.REGULAR:
                    return UnityEngine.FontStyle.Normal;

                case Flare.Text.FontStyle.BOLD_ITALIC:
                    return UnityEngine.FontStyle.BoldAndItalic;

                case Flare.Text.FontStyle.BOLD:
                    return UnityEngine.FontStyle.Bold;

                case Flare.Text.FontStyle.ITALIC:
                    return UnityEngine.FontStyle.Italic;

                default:
                    return UnityEngine.FontStyle.Normal;
            }
        }
    }
}
