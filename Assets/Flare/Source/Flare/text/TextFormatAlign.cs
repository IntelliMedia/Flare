//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

namespace Flare.Text
{
    /// <summary>
    /// Constants used to set the align property of TextFormat objects.
    /// </summary>
    public class TextFormatAlign
    {
        /// <summary>
        /// Indicates text should be centered in the text field.
        /// </summary>
        public const string CENTER = "center";

        /// <summary>
        /// Indicates text should be justified in the text field.
        /// </summary>
        public const string JUSTIFY = "justify";

        /// <summary>
        /// Indicates text should be aligned to the left in the text field.
        /// </summary>
        public const string LEFT = "left";

        /// <summary>
        /// Indicates text should be aligned to the right in the text field.
        /// </summary>
        public const string RIGHT = "right";

        /// <summary>
        /// Determines whether align is a member of TextFormatAlign.
        /// </summary>
        public static bool IsMember(string align)
        {
            switch (align)
            {
                case CENTER:
                case JUSTIFY:
                case LEFT:
                case RIGHT:
                    return true;

                default:
                    return false;
            }
        }
    }
}
