﻿//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

namespace Flare.UI
{
    /// <summary>
    /// The Keyboard class defines key code constants representing keys on the keyboard
    /// as well as key location constants that indicate the location of a key.
    /// </summary>
    public class Keyboard
    {
        /// <summary>
        /// The KeyLocation enum defines constants indicating the location of a key
        /// pressed on the keyboard.
        /// </summary>
        public enum KeyLocation
        {
            /// <summary>
            /// Indicates the key does not have a left or right version and did not originate
            /// from the numeric keypad.
            /// </summary>
            STANDARD = 0,
            
            /// <summary>
            /// Indicates the key originates from the left key location (e.g., Left-Shift).
            /// </summary>
            LEFT = 1,
            
            /// <summary>
            /// Indicates the key originates from the right key location (e.g., Right-Shift).
            /// </summary>
            RIGHT = 2,
            
            /// <summary>
            /// Indicates the key originates from the numeric keypad.
            /// </summary>
            NUM_PAD = 3
        }

        /// <summary>
        /// The KeyCode enum defines constants representing keys on the keyboard.
        /// </summary>
        public enum KeyCode
        {
            /// <summary>
            /// Constant for the Backspace key.
            /// </summary>
            BACKSPACE = 8,

            /// <summary>
            /// Constant for the Tab key.
            /// </summary>
            TAB = 9,

            /// <summary>
            /// Constant for the Enter key.
            /// </summary>
            ENTER = 13,

            /// <summary>
            /// Constant for the Command key.
            /// </summary>
            COMMAND = 15,

            /// <summary>
            /// Constant for the Shift key.
            /// </summary>
            SHIFT = 16,

            /// <summary>
            /// Constant for the Ctrl key.
            /// </summary>
            CONTROL = 17,

            /// <summary>
            /// Constant for the Alt key.
            /// </summary>
            ALTERNATE = 18,

            /// <summary>
            /// Constant for the Caps Lock key.
            /// </summary>
            CAPS_LOCK = 20,

            /// <summary>
            /// Constant for the Espace key.
            /// </summary>
            ESCAPE = 27,

            /// <summary>
            /// Constant for the Spacebar key.
            /// </summary>
            SPACE = 32,

            /// <summary>
            /// Constant for the Page Up key.
            /// </summary>
            PAGE_UP = 33,

            /// <summary>
            /// Constant for the Page Down key.
            /// </summary>
            PAGE_DOWN = 34,

            /// <summary>
            /// Constant for the End key.
            /// </summary>
            END = 35,

            /// <summary>
            /// Constant for the Home key.
            /// </summary>
            HOME = 36,

            /// <summary>
            /// Constant for the Left key.
            /// </summary>
            LEFT = 37,

            /// <summary>
            /// Constant for the Up key.
            /// </summary>
            UP = 38,

            /// <summary>
            /// Constant for the Right key.
            /// </summary>
            RIGHT = 39,

            /// <summary>
            /// Constant for the Down key.
            /// </summary>
            DOWN = 40,

            /// <summary>
            /// Constant for the Insert key.
            /// </summary>
            INSERT = 45,

            /// <summary>
            /// Constant for the Delete key.
            /// </summary>
            DELETE = 46,

            /// <summary>
            /// Constant for the '0' key.
            /// </summary>
            NUMBER_0 = 48,
            
            /// <summary>
            /// Constant for the '1' key.
            /// </summary>
            NUMBER_1 = 49,
            
            /// <summary>
            /// Constant for the '2' key.
            /// </summary>
            NUMBER_2 = 50,
            
            /// <summary>
            /// Constant for the '3' key.
            /// </summary>
            NUMBER_3 = 51,
            
            /// <summary>
            /// Constant for the '4' key.
            /// </summary>
            NUMBER_4 = 52,
            
            /// <summary>
            /// Constant for the '5' key.
            /// </summary>
            NUMBER_5 = 53,
            
            /// <summary>
            /// Constant for the '6' key.
            /// </summary>
            NUMBER_6 = 54,
            
            /// <summary>
            /// Constant for the '7' key.
            /// </summary>
            NUMBER_7 = 55,
            
            /// <summary>
            /// Constant for the '8' key.
            /// </summary>
            NUMBER_8 = 56,
            
            /// <summary>
            /// Constant for the '9' key.
            /// </summary>
            NUMBER_9 = 57,

            /// <summary>
            /// Constant for the 'A' key.
            /// </summary>
            A = 65,

            /// <summary>
            /// Constant for the 'B' key.
            /// </summary>
            B = 66,
            
            /// <summary>
            /// Constant for the 'C' key.
            /// </summary>
            C = 67,
            
            /// <summary>
            /// Constant for the 'D' key.
            /// </summary>
            D = 68,
            
            /// <summary>
            /// Constant for the 'E' key.
            /// </summary>
            E = 69,
            
            /// <summary>
            /// Constant for the 'F' key.
            /// </summary>
            F = 70,
            
            /// <summary>
            /// Constant for the 'G' key.
            /// </summary>
            G = 71,
            
            /// <summary>
            /// Constant for the 'H' key.
            /// </summary>
            H = 72,
            
            /// <summary>
            /// Constant for the 'I' key.
            /// </summary>
            I = 73,
            
            /// <summary>
            /// Constant for the 'J' key.
            /// </summary>
            J = 74,
            
            /// <summary>
            /// Constant for the 'K' key.
            /// </summary>
            K = 75,
            
            /// <summary>
            /// Constant for the 'L' key.
            /// </summary>
            L = 76,
            
            /// <summary>
            /// Constant for the 'M' key.
            /// </summary>
            M = 77,
            
            /// <summary>
            /// Constant for the 'N' key.
            /// </summary>
            N = 78,
            
            /// <summary>
            /// Constant for the 'O' key.
            /// </summary>
            O = 79,
            
            /// <summary>
            /// Constant for the 'P' key.
            /// </summary>
            P = 80,
            
            /// <summary>
            /// Constant for the 'Q' key.
            /// </summary>
            Q = 81,
            
            /// <summary>
            /// Constant for the 'R' key.
            /// </summary>
            R = 82,
            
            /// <summary>
            /// Constant for the 'S' key.
            /// </summary>
            S = 83,
            
            /// <summary>
            /// Constant for the 'T' key.
            /// </summary>
            T = 84,
            
            /// <summary>
            /// Constant for the 'U' key.
            /// </summary>
            U = 85,
            
            /// <summary>
            /// Constant for the 'V' key.
            /// </summary>
            V = 86,
            
            /// <summary>
            /// Constant for the 'W' key.
            /// </summary>
            W = 87,
            
            /// <summary>
            /// Constant for the 'X' key.
            /// </summary>
            X = 88,
            
            /// <summary>
            /// Constant for the 'Y' key.
            /// </summary>
            Y = 89,
            
            /// <summary>
            /// Constant for the 'Z' key.
            /// </summary>
            Z = 90,

            /// <summary>
            /// Constant for the '0' key on the numeric keypad.
            /// </summary>
            NUMPAD_0 = 96,

            /// <summary>
            /// Constant for the '1' key on the numeric keypad.
            /// </summary>
            NUMPAD_1 = 97,

            /// <summary>
            /// Constant for the '2' key on the numeric keypad.
            /// </summary>
            NUMPAD_2 = 98,

            /// <summary>
            /// Constant for the '3' key on the numeric keypad.
            /// </summary>
            NUMPAD_3 = 99,

            /// <summary>
            /// Constant for the '4' key on the numeric keypad.
            /// </summary>
            NUMPAD_4 = 100,

            /// <summary>
            /// Constant for the '5' key on the numeric keypad.
            /// </summary>
            NUMPAD_5 = 101,

            /// <summary>
            /// Constant for the '6' key on the numeric keypad.
            /// </summary>
            NUMPAD_6 = 102,

            /// <summary>
            /// Constant for the '7' key on the numeric keypad.
            /// </summary>
            NUMPAD_7 = 103,

            /// <summary>
            /// Constant for the '8' key on the numeric keypad.
            /// </summary>
            NUMPAD_8 = 104,

            /// <summary>
            /// Constant for the '9' key on the numeric keypad.
            /// </summary>
            NUMPAD_9 = 105,

            /// <summary>
            /// Constant for the '+' key on the numeric keypad.
            /// </summary>
            NUMPAD_MULTIPLY = 106,

            /// <summary>
            /// Constant for the '+' key on the numeric keypad.
            /// </summary>
            NUMPAD_ADD = 107,
            
            /// <summary>
            /// Constant for the '+' key on the numeric keypad.
            /// </summary>
            NUMPAD_ENTER = 108,

            /// <summary>
            /// Constant for the '+' key on the numeric keypad.
            /// </summary>
            NUMPAD_SUBTRACT = 109,

            /// <summary>
            /// Constant for the '+' key on the numeric keypad.
            /// </summary>
            NUMPAD_DECIMAL = 110,

            /// <summary>
            /// Constant for the '+' key on the numeric keypad.
            /// </summary>
            NUMPAD_DIVIDE = 111,

            /// <summary>
            /// Constant for the 'F1' key on the numeric keypad.
            /// </summary>
            F1 = 112,
            
            /// <summary>
            /// Constant for the 'F2' key on the numeric keypad.
            /// </summary>
            F2 = 113,
            
            /// <summary>
            /// Constant for the 'F3' key on the numeric keypad.
            /// </summary>
            F3 = 114,
            
            /// <summary>
            /// Constant for the 'F4' key on the numeric keypad.
            /// </summary>
            F4 = 115,
            
            /// <summary>
            /// Constant for the 'F5' key on the numeric keypad.
            /// </summary>
            F5 = 116,
            
            /// <summary>
            /// Constant for the 'F6' key on the numeric keypad.
            /// </summary>
            F6 = 117,
            
            /// <summary>
            /// Constant for the 'F7' key on the numeric keypad.
            /// </summary>
            F7 = 118,
            
            /// <summary>
            /// Constant for the 'F8' key on the numeric keypad.
            /// </summary>
            F8 = 119,
            
            /// <summary>
            /// Constant for the 'F9' key on the numeric keypad.
            /// </summary>
            F9 = 120,
            
            /// <summary>
            /// Constant for the 'F10' key on the numeric keypad.
            /// </summary>
            F10 = 121,
            
            /// <summary>
            /// Constant for the 'F11' key on the numeric keypad.
            /// </summary>
            F11 = 122,
            
            /// <summary>
            /// Constant for the 'F12' key on the numeric keypad.
            /// </summary>
            F12 = 123,
            
            /// <summary>
            /// Constant for the 'F13' key on the numeric keypad.
            /// </summary>
            F13 = 124,
            
            /// <summary>
            /// Constant for the 'F14' key on the numeric keypad.
            /// </summary>
            F14 = 125,
            
            /// <summary>
            /// Constant for the 'F15' key on the numeric keypad.
            /// </summary>
            F15 = 126,

            /// <summary>
            /// Constant for the ';' key.
            /// </summary>
            SEMICOLON = 186,

            /// <summary>
            /// Constant for the '=' key.
            /// </summary>
            EQUAL = 187,

            /// <summary>
            /// Constant for the ',' key.
            /// </summary>
            COMMA = 188,

            /// <summary>
            /// Constant for the '-' key.
            /// </summary>
            MINUS = 189,

            /// <summary>
            /// Constant for the Period key.
            /// </summary>
            PERIOD = 190,

            /// <summary>
            /// Constant for the '/' key.
            /// </summary>
            SLASH = 191,

            /// <summary>
            /// Constant for the '`' key.
            /// </summary>
            BACKQUOTE = 192,

            /// <summary>
            /// Constant for the '[' key.
            /// </summary>
            LEFTBRACKET = 219,

            /// <summary>
            /// Constant for the '\' key.
            /// </summary>
            BACKSLASH = 220,

            /// <summary>
            /// Constant for the ']' key.
            /// </summary>
            RIGHTBRACKET = 221,

            /// <summary>
            /// Constant for the Quote key.
            /// </summary>
            QUOTE = 222
        }

        public struct KeyCodeMap
        {
            public UnityEngine.KeyCode unityCode { get; private set; }
            public KeyCode flareCode { get; private set; }
            public KeyLocation keyLocation { get; private set; }
            public char charCode { get; private set; }
            public char shiftedCharCode { get; private set; }
            
            public KeyCodeMap(UnityEngine.KeyCode unityCode, KeyCode flareCode, 
                KeyLocation keyLocation, char charCode, char shiftedCharCode)
            {
                this.flareCode = flareCode;
                this.unityCode = unityCode;
                this.charCode = charCode;
                this.keyLocation = keyLocation;
                this.shiftedCharCode = shiftedCharCode;
            }
        }

        public static KeyCodeMap[] KeyCodeMaps
        {
            get { return ms_keyCodeMaps; }
        }

        private static KeyCodeMap[] ms_keyCodeMaps = new KeyCodeMap[]
        {
            new KeyCodeMap(UnityEngine.KeyCode.Backspace, KeyCode.BACKSPACE, KeyLocation.STANDARD, '\b', '\b'),
            new KeyCodeMap(UnityEngine.KeyCode.Tab, KeyCode.TAB, KeyLocation.STANDARD, '\t', '\t'),
            new KeyCodeMap(UnityEngine.KeyCode.Return, KeyCode.ENTER, KeyLocation.STANDARD, '\n', '\n'),
            new KeyCodeMap(UnityEngine.KeyCode.LeftCommand, KeyCode.COMMAND, KeyLocation.LEFT, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.RightCommand, KeyCode.COMMAND, KeyLocation.RIGHT, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.LeftShift, KeyCode.SHIFT, KeyLocation.LEFT, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.RightShift, KeyCode.SHIFT, KeyLocation.RIGHT, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.LeftControl, KeyCode.CONTROL, KeyLocation.LEFT, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.RightControl, KeyCode.CONTROL, KeyLocation.RIGHT, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.LeftAlt, KeyCode.ALTERNATE, KeyLocation.LEFT, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.RightAlt, KeyCode.ALTERNATE, KeyLocation.RIGHT, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.CapsLock, KeyCode.CAPS_LOCK, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.Escape, KeyCode.ESCAPE, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.Space, KeyCode.SPACE, KeyLocation.STANDARD, ' ', ' '),
            new KeyCodeMap(UnityEngine.KeyCode.PageUp, KeyCode.PAGE_UP, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.PageDown, KeyCode.PAGE_DOWN, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.End, KeyCode.END, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.Home, KeyCode.HOME, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.LeftArrow, KeyCode.LEFT, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.UpArrow, KeyCode.UP, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.RightArrow, KeyCode.RIGHT, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.DownArrow, KeyCode.DOWN, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.Insert, KeyCode.INSERT, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.Delete, KeyCode.DELETE, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.Alpha0, KeyCode.NUMBER_0, KeyLocation.STANDARD, '0', ')'),
            new KeyCodeMap(UnityEngine.KeyCode.Alpha1, KeyCode.NUMBER_1, KeyLocation.STANDARD, '1', '!'),
            new KeyCodeMap(UnityEngine.KeyCode.Alpha2, KeyCode.NUMBER_2, KeyLocation.STANDARD, '2', '@'),
            new KeyCodeMap(UnityEngine.KeyCode.Alpha3, KeyCode.NUMBER_3, KeyLocation.STANDARD, '3', '#'),
            new KeyCodeMap(UnityEngine.KeyCode.Alpha4, KeyCode.NUMBER_4, KeyLocation.STANDARD, '4', '$'),
            new KeyCodeMap(UnityEngine.KeyCode.Alpha5, KeyCode.NUMBER_5, KeyLocation.STANDARD, '5', '%'),
            new KeyCodeMap(UnityEngine.KeyCode.Alpha6, KeyCode.NUMBER_6, KeyLocation.STANDARD, '6', '^'),
            new KeyCodeMap(UnityEngine.KeyCode.Alpha7, KeyCode.NUMBER_7, KeyLocation.STANDARD, '7', '&'),
            new KeyCodeMap(UnityEngine.KeyCode.Alpha8, KeyCode.NUMBER_8, KeyLocation.STANDARD, '8', '*'),
            new KeyCodeMap(UnityEngine.KeyCode.Alpha9, KeyCode.NUMBER_9, KeyLocation.STANDARD, '9', '('),
            new KeyCodeMap(UnityEngine.KeyCode.A, KeyCode.A, KeyLocation.STANDARD, 'a', 'A'),
            new KeyCodeMap(UnityEngine.KeyCode.B, KeyCode.B, KeyLocation.STANDARD, 'b', 'B'),
            new KeyCodeMap(UnityEngine.KeyCode.C, KeyCode.C, KeyLocation.STANDARD, 'c', 'C'),
            new KeyCodeMap(UnityEngine.KeyCode.D, KeyCode.D, KeyLocation.STANDARD, 'd', 'D'),
            new KeyCodeMap(UnityEngine.KeyCode.E, KeyCode.E, KeyLocation.STANDARD, 'e', 'E'),
            new KeyCodeMap(UnityEngine.KeyCode.F, KeyCode.F, KeyLocation.STANDARD, 'f', 'F'),
            new KeyCodeMap(UnityEngine.KeyCode.G, KeyCode.G, KeyLocation.STANDARD, 'g', 'G'),
            new KeyCodeMap(UnityEngine.KeyCode.H, KeyCode.H, KeyLocation.STANDARD, 'h', 'H'),
            new KeyCodeMap(UnityEngine.KeyCode.I, KeyCode.I, KeyLocation.STANDARD, 'i', 'I'),
            new KeyCodeMap(UnityEngine.KeyCode.J, KeyCode.J, KeyLocation.STANDARD, 'j', 'J'),
            new KeyCodeMap(UnityEngine.KeyCode.K, KeyCode.K, KeyLocation.STANDARD, 'k', 'K'),
            new KeyCodeMap(UnityEngine.KeyCode.L, KeyCode.L, KeyLocation.STANDARD, 'l', 'L'),
            new KeyCodeMap(UnityEngine.KeyCode.M, KeyCode.M, KeyLocation.STANDARD, 'm', 'M'),
            new KeyCodeMap(UnityEngine.KeyCode.N, KeyCode.N, KeyLocation.STANDARD, 'n', 'N'),
            new KeyCodeMap(UnityEngine.KeyCode.O, KeyCode.O, KeyLocation.STANDARD, 'o', 'O'),
            new KeyCodeMap(UnityEngine.KeyCode.P, KeyCode.P, KeyLocation.STANDARD, 'p', 'P'),
            new KeyCodeMap(UnityEngine.KeyCode.Q, KeyCode.Q, KeyLocation.STANDARD, 'q', 'Q'),
            new KeyCodeMap(UnityEngine.KeyCode.R, KeyCode.R, KeyLocation.STANDARD, 'r', 'R'),
            new KeyCodeMap(UnityEngine.KeyCode.S, KeyCode.S, KeyLocation.STANDARD, 's', 'S'),
            new KeyCodeMap(UnityEngine.KeyCode.T, KeyCode.T, KeyLocation.STANDARD, 't', 'T'),
            new KeyCodeMap(UnityEngine.KeyCode.U, KeyCode.U, KeyLocation.STANDARD, 'u', 'U'),
            new KeyCodeMap(UnityEngine.KeyCode.V, KeyCode.V, KeyLocation.STANDARD, 'v', 'V'),
            new KeyCodeMap(UnityEngine.KeyCode.W, KeyCode.W, KeyLocation.STANDARD, 'w', 'W'),
            new KeyCodeMap(UnityEngine.KeyCode.X, KeyCode.X, KeyLocation.STANDARD, 'x', 'X'),
            new KeyCodeMap(UnityEngine.KeyCode.Y, KeyCode.Y, KeyLocation.STANDARD, 'y', 'Y'),
            new KeyCodeMap(UnityEngine.KeyCode.Z, KeyCode.Z, KeyLocation.STANDARD, 'z', 'Z'),
            new KeyCodeMap(UnityEngine.KeyCode.Keypad0, KeyCode.NUMPAD_0, KeyLocation.NUM_PAD, '0', '0'),
            new KeyCodeMap(UnityEngine.KeyCode.Keypad1, KeyCode.NUMPAD_1, KeyLocation.NUM_PAD, '1', '1'),
            new KeyCodeMap(UnityEngine.KeyCode.Keypad2, KeyCode.NUMPAD_2, KeyLocation.NUM_PAD, '2', '2'),
            new KeyCodeMap(UnityEngine.KeyCode.Keypad3, KeyCode.NUMPAD_3, KeyLocation.NUM_PAD, '3', '3'),
            new KeyCodeMap(UnityEngine.KeyCode.Keypad4, KeyCode.NUMPAD_4, KeyLocation.NUM_PAD, '4', '4'),
            new KeyCodeMap(UnityEngine.KeyCode.Keypad5, KeyCode.NUMPAD_5, KeyLocation.NUM_PAD, '5', '5'),
            new KeyCodeMap(UnityEngine.KeyCode.Keypad6, KeyCode.NUMPAD_6, KeyLocation.NUM_PAD, '6', '6'),
            new KeyCodeMap(UnityEngine.KeyCode.Keypad7, KeyCode.NUMPAD_7, KeyLocation.NUM_PAD, '7', '7'),
            new KeyCodeMap(UnityEngine.KeyCode.Keypad8, KeyCode.NUMPAD_8, KeyLocation.NUM_PAD, '8', '8'),
            new KeyCodeMap(UnityEngine.KeyCode.Keypad9, KeyCode.NUMPAD_9, KeyLocation.NUM_PAD, '9', '9'),
            new KeyCodeMap(UnityEngine.KeyCode.KeypadMultiply, KeyCode.NUMPAD_MULTIPLY, KeyLocation.NUM_PAD, '*', '*'),
            new KeyCodeMap(UnityEngine.KeyCode.KeypadPlus, KeyCode.NUMPAD_ADD, KeyLocation.NUM_PAD, '+', '+'),
            new KeyCodeMap(UnityEngine.KeyCode.KeypadEnter, KeyCode.NUMPAD_ENTER, KeyLocation.NUM_PAD, '\n', '\n'),
            new KeyCodeMap(UnityEngine.KeyCode.KeypadMinus, KeyCode.NUMPAD_SUBTRACT, KeyLocation.NUM_PAD, '-', '-'),
            new KeyCodeMap(UnityEngine.KeyCode.KeypadPeriod, KeyCode.NUMPAD_DECIMAL, KeyLocation.NUM_PAD, '.', '.'),
            new KeyCodeMap(UnityEngine.KeyCode.KeypadDivide, KeyCode.NUMPAD_DIVIDE, KeyLocation.NUM_PAD, '/', '/'),
            new KeyCodeMap(UnityEngine.KeyCode.F1, KeyCode.F1, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.F2, KeyCode.F2, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.F3, KeyCode.F3, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.F4, KeyCode.F4, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.F5, KeyCode.F5, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.F6, KeyCode.F6, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.F7, KeyCode.F7, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.F8, KeyCode.F8, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.F9, KeyCode.F9, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.F10, KeyCode.F10, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.F11, KeyCode.F11, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.F12, KeyCode.F12, KeyLocation.STANDARD, '\0', '\0'),
            new KeyCodeMap(UnityEngine.KeyCode.Semicolon, KeyCode.SEMICOLON, KeyLocation.STANDARD, ';', ':'),
            new KeyCodeMap(UnityEngine.KeyCode.Equals, KeyCode.EQUAL, KeyLocation.STANDARD, '=', '+'),
            new KeyCodeMap(UnityEngine.KeyCode.Comma, KeyCode.COMMA, KeyLocation.STANDARD, ',', '<'),
            new KeyCodeMap(UnityEngine.KeyCode.Minus, KeyCode.MINUS, KeyLocation.STANDARD, '-', '_'),
            new KeyCodeMap(UnityEngine.KeyCode.Period, KeyCode.PERIOD, KeyLocation.STANDARD, '.', '>'),
            new KeyCodeMap(UnityEngine.KeyCode.Slash, KeyCode.SLASH, KeyLocation.STANDARD, '/', '?'),
            new KeyCodeMap(UnityEngine.KeyCode.BackQuote, KeyCode.BACKQUOTE, KeyLocation.STANDARD, '`', '~'),
            new KeyCodeMap(UnityEngine.KeyCode.LeftBracket, KeyCode.LEFTBRACKET, KeyLocation.STANDARD, '[', '{'),
            new KeyCodeMap(UnityEngine.KeyCode.Backslash, KeyCode.BACKSLASH, KeyLocation.STANDARD, '\\', '|'),
            new KeyCodeMap(UnityEngine.KeyCode.RightBracket, KeyCode.RIGHTBRACKET, KeyLocation.STANDARD, ']', '}'),
            new KeyCodeMap(UnityEngine.KeyCode.Quote, KeyCode.QUOTE, KeyLocation.STANDARD, '\'', '"'),
        };
    }
}
