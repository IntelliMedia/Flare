﻿//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;

namespace Flare.Sys
{
    /// <summary>
    /// Provides access to the Unity input system. An InputAccessor instance allows
    /// Flare to access user input from Unity. The default implementation maps directly
    /// to the corresponding Unity Input members; however, the class can be overridden
    /// to create modified behaviors (e.g., playing back recorded input).
    /// </summary>
    public class InputAccessor
    {   
        /// <summary>
        /// Returns true if any key or mouse button is currently held down.
        /// </summary>
        public virtual bool anyKey
        {
            get { return Input.anyKey; }
        }

        /// <summary>
        /// Returns true the first frame the user hits any key or mouse button.
        /// </summary>
        public virtual bool anyKeyDown
        {
            get { return Input.anyKeyDown; }
        }

        /// <summary>
        /// Returns mouse position in pixel coordinates.
        /// </summary>
        public virtual Vector3 mousePosition
        {
            get { return Input.mousePosition; }
        }

        /// <summary>
        /// Returns true while the user holds the identified key.
        /// </summary>
        public virtual bool GetKey(KeyCode key)
        {
            return Input.GetKey(key);
        }

        /// <summary>
        /// Returns true during the frame the user presses the identified key.
        /// </summary>
        public virtual bool GetKeyDown(KeyCode key)
        {
            return Input.GetKeyDown(key);
        }

        /// <summary>
        /// Returns true during the frame the user releases the identified key.
        /// </summary>
        public virtual bool GetKeyUp(KeyCode key)
        {
            return Input.GetKeyUp(key);
        }

        /// <summary>
        /// Returns true if the identified mouse button is pressed.
        /// </summary>
        public virtual bool GetMouseButton(int button)
        {
            return Input.GetMouseButton(button);
        }

        /// <summary>
        /// Returns true during the frame the user presses the identified mouse button.
        /// </summary>
        public virtual bool GetMouseButtonDown(int button)
        {
            return Input.GetMouseButtonDown(button);
        }

        /// <summary>
        /// Returns true during the frame the user releases the identified mouse button.
        /// </summary>
        public virtual bool GetMouseButtonUp(int button)
        {
            return Input.GetMouseButtonUp(button);
        }
    }
}
