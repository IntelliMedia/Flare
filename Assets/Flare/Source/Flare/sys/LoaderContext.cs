﻿//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

using System.Collections.Generic;

using Flare.Display;

namespace Flare.Sys
{
    /// <summary>
    /// The LoaderContext class provides a context for the Loader class to use for
    /// loading SWF files using the Load method.
    /// </summary>
    public class LoaderContext
    {
        /// <summary>
        /// Gets the application domain associated with the loader context
        /// </summary>
        public ApplicationDomain applicationDomain { get; private set; }

        /// <summary>
        /// Repository of characters defined in the loaded SWF file
        /// </summary>
        public Dictionary<int, object> dictionary { get; private set; }

        /// <summary>
        /// Initializes a new instance of the LoaderContext class. If no application domain
        /// is specified then a new child domain of the current application domain will be used.
        /// </summary>
        public LoaderContext(ApplicationDomain applicationDomain = null)
        {
            // Setup the application domain
            this.applicationDomain = (applicationDomain != null) ? 
                applicationDomain : new ApplicationDomain(ApplicationDomain.currentDomain);

            // Create the character dictionary
            this.dictionary = new Dictionary<int, object>();
        }
    }
}
