//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The SwfAsset class maintains an imported SWF file. The contents of the
/// SWF file can be loaded using the Bytes property of the SwfAsset object.
/// </summary>
[Serializable]
public class SwfAsset : ScriptableObject
{
    public readonly static int CurrentVersionNumber = 1;

    [SerializeField]
    private int version = 0;

    [SerializeField]
    private byte[] bytes;

    [Serializable]
    public struct BitmapIdAndTexture
    {
        public int id;
        public Texture2D texture;

        public BitmapIdAndTexture(int id, Texture2D texture)
        {
            this.id = id;
            this.texture = texture;
        }
    }

    [Serializable]
    public struct SpriteIdAndMeshes
    {
        public int id;
        public Mesh[] meshes;

        public SpriteIdAndMeshes(int id, Mesh[] meshes)
        {
            this.id = id;
            this.meshes = meshes;
        }
    }

    [SerializeField]
    private BitmapIdAndTexture[] bitmapIdAndTextureArray;

    [SerializeField]
    private SpriteIdAndMeshes[] spriteIdAndMeshesArray;

    /// <summary>
    /// Gets or sets the bytes associated with the SwfAsset.
    /// </summary>
    public byte[] Bytes
    {
        get { return bytes; }
        set { bytes = value; }
    }

    /// <summary>
    /// Gets or sets the version of the SwfAsset.
    /// </summary>
    public int Version
    {
        get { return version; }
        set { version = value; }
    }

    /// <summary>
    /// Gets or sets the textures associated with the SwfAsset.
    /// </summary>
    public BitmapIdAndTexture[] BitmapIdAndTextureArray
    {
        get { return bitmapIdAndTextureArray; }
        set { bitmapIdAndTextureArray = value; }
    }

    /// <summary>
    /// Gets or sets the meshes associated with the SwfAsset.
    /// </summary>
    public SpriteIdAndMeshes[] SpriteIdAndMeshesArray
    {
        get { return spriteIdAndMeshesArray; }
        set { spriteIdAndMeshesArray = value; }
    }

    public void OnEnable()
    {
    }

    public void OnDisable()
    {
    }
}
