//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

using Flare.Geom;

namespace Flare.Display
{
    /// <summary>
    /// The GraphicsBitmapFill class defines a bitmap fill. The bitmap fill can
    /// be repeated or smoothed as well as transformed using a matrix.
    /// </summary>
    public class GraphicsBitmapFill
    {
        /// <summary>
        /// Bitmap image for the fill.
        /// </summary>
        public BitmapData bitmapData { get; set; }

        /// <summary>
        /// Transformation matrix for the bitmap fill.
        /// </summary>
        public Matrix matrix { get; set; }

        /// <summary>
        /// Indicates if the bitmap should be tiled or not.
        /// </summary>
        public bool repeat { get; set; }

        /// <summary>
        /// Indicates if the bitmap should be smoothed or not.
        /// </summary>
        public bool smooth { get; set; }

        /// <summary>
        /// Create a bitmap fill using the specified values.
        /// </summary>
        public GraphicsBitmapFill(BitmapData bitmapData = null,
            Matrix matrix = null, bool repeat = true, bool smooth = false)
        {
            this.bitmapData = bitmapData;
            this.matrix = matrix;
            this.repeat = repeat;
            this.smooth = smooth;
        }
    }
}