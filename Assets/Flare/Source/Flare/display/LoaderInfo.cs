//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

using System.Collections.Generic;

using Flare.Geom;
using Flare.Sys;

namespace Flare.Display
{
    /// <summary>
    /// The LoaderInfo class maintains information about a loaded SWF file.
    /// </summary>
    public class LoaderInfo
    {
        // TODO bwmott 2014-02-14: Accessing most of these fields prior to loading should
        // throw an exception.

        /// <summary>
        /// Script version used by the loaded SWF file.
        /// </summary>
        public uint scriptVersion { get; internal set; }

        /// <summary>
        /// Application domain that maintains the list of exported symbols for the loaded SWF file.
        /// </summary>
        public ApplicationDomain applicationDomain { get { return context.applicationDomain; } }

        /// <summary>
        /// Number of bytes loaded.
        /// </summary>
        public uint bytesLoaded { get; internal set; }

        /// <summary>
        /// Total number of bytes in the SWF file.
        /// </summary>
        public uint bytesTotal { get; internal set; }

        /// <summary>
        /// Loaded display object associated with this loader info object.
        /// </summary>
        public DisplayObject content { get; internal set; }

        /// <summary>
        /// MIME type of the loaded file (application/x-shockwave-flash).
        /// </summary>
        public string contentType { get; internal set; }

        /// <summary>
        /// Desired frame rate, in frames per second, for the loaded SWF file.
        /// </summary>
        public float frameRate { get; internal set; }

        /// <summary>
        /// Height of the loaded SWF file.
        /// </summary>
        public int height { get; internal set; }

        /// <summary>
        /// Loader object associated with this loader info object.
        /// </summary>
        public Loader loader { get; internal set; }

        /// <summary>
        /// File format version of the loaded SWF file.
        /// </summary>
        public uint swfVersion { get; internal set; }

        /// <summary>
        /// URL of the file being loaded.
        /// </summary>
        public string url { get; internal set; }

        /// <summary>
        /// Width of the loaded SWF file.
        /// </summary>
        public int width { get; internal set; }

        internal LoaderContext context { get; set; }
        internal uint color { get; set; }
        internal int totalFrames { get; set; }

        /// <summary>
        /// Create a loader info object.
        /// </summary>
        public LoaderInfo()
        {
            this.scriptVersion = ScriptVersion.AS2;
        }
    }
}
