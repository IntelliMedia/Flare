//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

using UnityEngine;

namespace Flare.Display
{
    /// <summary>
    /// The BitmapData class wraps a Unity Texture2D object. The class provides access to basic
    /// attributes of the texture including its width, height, and transparency. Instances of
    /// the class are primarily used to create display objects with a bitmap fill.
    /// </summary>
    public class BitmapData
    {
        /// <summary>
        /// Returns the texture associated with the bitmap.
        /// </summary>
        public Texture2D texture { get;
            /* \cond */ private set; /* \endcond */ }

        /// <summary>
        /// Gets the height of the bitmap in pixels.
        /// </summary>
        public int height
        {
            get { return texture.height; }
        }

        /// <summary>
        /// Gets the width of the bitmap in pixels.
        /// </summary>
        public int width
        {
            get { return texture.width; }
        }

        /// <summary>
        /// Return true if the bitmap is transparent; otherwise, false.
        /// </summary>
        public bool transparent
        {
            get { return (texture.format == TextureFormat.ARGB4444)
                || (texture.format == TextureFormat.ARGB32)
                || (texture.format == TextureFormat.DXT5)
                || (texture.format == TextureFormat.RGBA4444); }
        }

        /// <summary>
        /// Create a new BitmapData object using the given texture.
        /// </summary>
        /// <param name="texture">Unity2D texture to be associated with the bitmap.</param>
        public BitmapData(Texture2D texture)
        {
            this.texture = texture;
        }
    }
}
