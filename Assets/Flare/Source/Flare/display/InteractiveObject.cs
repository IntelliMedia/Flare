//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

namespace Flare.Display
{
    /// <summary>
    /// The InteractiveObject class is an abstract base class for display objects that
    /// support user interaction using the mouse or other input device.
    /// </summary>
    public class InteractiveObject : DisplayObject
    {
        /// <summary>
        /// Indicates if this object receives mouse or other input messages.
        /// </summary>
        public virtual bool mouseEnabled { get; set; }

        /// <summary>
        /// Indicates if this object is in the tab order.
        /// </summary>
        public virtual bool tabEnabled { get; set; }

        /// <summary>
        /// Indicates the tab order for this object.
        /// </summary>
        public virtual int tabIndex { get; set; }

        internal InteractiveObject()
        {
            this.mouseEnabled = true;
            this.tabIndex = -1;
        }
    }
}
