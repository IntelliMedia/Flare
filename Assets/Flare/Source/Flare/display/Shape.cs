//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

using Flare.Geom;

namespace Flare.Display
{
    /// <summary>
    /// The Shape class can be used to create lightweight shapes using the drawing
    /// API provided by the Graphics class. The Shape class is similar to the Sprite
    /// class; however, instances of the Shape class cannot contain children and
    /// do not support user input.
    /// </summary>
    public class Shape : DisplayObject
    {
        /// <summary>
        /// Graphics object associated with the shape.
        /// </summary>
        public Graphics graphics { get;
            /* \cond */ internal set; /* \endcond */ }

        /// <summary>
        /// Create a new instance of the Shape class.
        /// </summary>
        public Shape()
        {
            this.graphics = new Graphics();
        }

        internal override bool HitTest(float x, float y, bool shapeFlag, bool evaluateChildren)
        {
            // Check the shape's graphics to see if there's a hit there or not
            return this.graphics.HitTest(GlobalToLocal(new Point(x, y)), shapeFlag);
        }

        internal override void Render(RenderContext context)
        {
            if (!this.visible || this.isMask)
            {
                return;
            }
            
            if (this.mask != null)
            {
                context.PushMask(this.mask);
            }

            if (this.graphics.ContainsPaths)
            {
                context.SetupMasks();

                this.graphics.Render(transform.concatenatedMatrix,
                    transform.concatenatedColorTransform);
            }

            if (this.mask != null)
            {
                context.PopMask();
            }
        }

        internal override void RenderAsMask(RenderContext context)
        {
            this.graphics.RenderAsMask(transform.concatenatedMatrix);
        }

        /// <summary>
        /// Return a clone of the Shape with a deep copy of any mutable members.
        /// </summary>
        public override object Clone()
        {
            Shape clone = (Shape)base.Clone();

            // Deep copy the graphics
            clone.graphics = new Graphics(this.graphics);

            return clone;
        }
    }
}