//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

namespace Flare.Display
{
    /// <summary>
    /// Constants used to specify path-drawing commands.
    /// </summary>
    public class GraphicsPathCommand
    {
        /// <summary>
        /// Indicates that a curve should be drawn from the current drawing position to the
        /// x- and y-coordinates specified in the graphics path data using a control point.
        /// </summary>
        public const int CURVE_TO = 3;

        /// <summary>
        /// Indicates that a line should be drawn from the current drawing position to the
        /// x- and y-coordinates specified in the graphics path data.
        /// </summary>
        public const int LINE_TO = 2;

        /// <summary>
        /// Indicates that the current drawing position should be moved to the x- and
        /// y-coordinates specified in the graphics path data.
        /// </summary>
        public const int MOVE_TO = 1;

        /// <summary>
        /// Indicates that no action should be performed.
        /// </summary>
        public const int NO_OP = 0;
    }
}
