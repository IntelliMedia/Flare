//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

using Flare.Geom;

namespace Flare.Display
{
    /// <summary>
    /// The GraphicsSolidFill class defines a solid fill.
    /// </summary>
    public class GraphicsSolidFill
    {
        /// <summary>
        /// Indicates the transparency of the fill where 0 is fully transparent and
        /// 1 is fully opaque.
        /// </summary>
        public float alpha { get; set; }

        /// <summary>
        /// RGB fill color encoded as a hexadecimal value (e.g., 0xFF0000 is red,
        /// 0x00FF00 is green, and 0x0000FF is blue).
        /// </summary>
        public uint color { get; set; }

        /// <summary>
        /// Create a solid fill using the specified values.
        /// </summary>
        public GraphicsSolidFill(uint color = 0, float alpha = 1.0f)
        {
            this.color = color;
            this.alpha = alpha;
        }

        public override string ToString ()
        {
             return string.Format("(color=0x{0:x6} alpha={1})", this.color, this.alpha);
        }
    }
}