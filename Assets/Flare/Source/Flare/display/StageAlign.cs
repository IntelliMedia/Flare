//-------------------------------------------------------------------------------------------------
// Copyright (c) Bradford W. Mott and Flare Contributors
// North Carolina State University, Department of Computer Science
// The IntelliMedia Group
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-------------------------------------------------------------------------------------------------

namespace Flare.Display
{
    /// <summary>
    /// Constants used to indicate alignment of the stage within the playback area.
    /// </summary>
    public class StageAlign
    {
        /// <summary>
        /// Indicates that the stage should be centered.
        /// </summary>
        public const string CENTER = "";

        /// <summary>
        /// Indicates that the stage should be aligned to the bottom.
        /// </summary>
        public const string BOTTOM = "B";

        /// <summary>
        /// Indicates that the stage should be aligned to the bottom-left.
        /// </summary>
        public const string BOTTOM_LEFT = "BL";

        /// <summary>
        /// Indicates that the stage should be aligned to the bottom-right.
        /// </summary>
        public const string BOTTOM_RIGHT = "BR";

        /// <summary>
        /// Indicates that the stage should be aligned to the left.
        /// </summary>
        public const string LEFT = "L";

        /// <summary>
        /// Indicates that the stage should be aligned to the right.
        /// </summary>
        public const string RIGHT = "R";

        /// <summary>
        /// Indicates that the stage should be aligned to the top.
        /// </summary>
        public const string TOP = "T";

        /// <summary>
        /// Indicates that the stage should be aligned to the top-left.
        /// </summary>
        public const string TOP_LEFT = "TL";

        /// <summary>
        /// Indicates that the stage should be aligned to the top-right.
        /// </summary>
        public const string TOP_RIGHT = "TR";
    }
}
