Flare is an open source toolkit for creating expressive user interfaces for
digital games with Unity. Flare is released under a simplifed BSD license.
Please see the 'License.txt' file for details.

It should be noted that Flare utilizes several third party packages which
have their own licenses (i.e., Ionic.Zlib, Poly2Tri, and the Liberation
fonts). The third party packages have flexible licenses; however, please
see the corresponding licenses for details on how they can be utilized.

The latest version of Flare can be found at:

  https://gitlab.com/IntelliMedia/Flare

For more information see the Wiki.

Enjoy,

The Flare Team

